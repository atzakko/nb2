#ifndef NBSERVER_HPP
#define NBSERVER_HPP

#include <iostream>
#include <string>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>

using boost::asio::ip::tcp;
typedef boost::shared_ptr<tcp::socket> socket_ptr;

class NbServer {
private:
    std::string last_error;
    void startServer(boost::asio::io_service& io_service, short port);
public:
    int init(int port);
    std::string getError();
};

#endif