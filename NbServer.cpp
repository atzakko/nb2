#include "NbServer.hpp"

using boost::asio::ip::tcp;

const int max_length = 1024;

//typedef boost::shared_ptr<tcp::socket> socket_ptr;

std::string NbServer::getError() {
  return this->last_error;
}

int NbServer::init(int port) {
    try
    {
      boost::asio::io_service io_service;
  
      using namespace std;
      this->startServer(io_service, port);
    }
    catch (std::exception& e)
    {
      this->last_error = e.what();
      return 1;
    }
    return 0;
}

int session(socket_ptr sock)
{
  try
  {
    for (;;)
    {

      char data[max_length];

      boost::system::error_code error;
      size_t length = sock->read_some(boost::asio::buffer(data), error);
      if (error == boost::asio::error::eof)
        break;
      else if (error)
        throw boost::system::system_error(error);

      std::string input(data,length);
      std::string zapros = std::string(data, length);
      std::string answer;
      if(zapros == "hz") {
        answer = "HZ HZ";
      } else
        answer = "NE ZNAY";//parser->parse(std::string(data,length));
      std::string output = answer;
      boost::asio::write(*sock, boost::asio::buffer(output.c_str(), output.length()));
      //boost::asio::write(*sock, boost::asio::buffer(data, length));

      //sock->close();
      //return 0;
    }
  }
  catch (std::exception& e)
  {
    return 1;
  }
  return 0;
}

void NbServer::startServer(boost::asio::io_service& io_service, short port) {
    tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
    for (;;)
    {
      socket_ptr sock(new tcp::socket(io_service));
      a.accept(*sock);
      boost::thread t(boost::bind(session, sock));
    }
}