# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Makefile
PROJECT_NAME=nb2
PROJECT_VERSION=1.0.00.0
PROJECT_RELEASE_TYPE=release

SOURCES=../src/main.cpp ../src/NbServer.cpp
obj_test=main.o NbServer.o

OBJECTS=$(SOURCES:.cpp=.o)

INCLUDE_DIRS=-IC:\Libraries\boost_1_65_1\include -IC:\Libraries\mysql\include

LIB_DIR=-LC:\Libraries\boost_1_65_1\lib -LC:\Libraries\mysql\lib
LIBS=-lboost_system -lboost_thread -lws2_32 -lmysql

EXE_NAME_WINDOWS=$(PROJECT_NAME:=.exe)

all:

windows: windows_compile windows_link windows_garbage_collect

windows_compile:
	g++ -c $(SOURCES) $(INCLUDE_DIRS)
	
windows_link:
	g++ $(obj_test) -o $(EXE_NAME_WINDOWS) $(LIB_DIR) $(LIBS)

windows_garbage_collect:
	del *.o
## MAKEFILE END