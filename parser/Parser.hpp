#ifndef PARSERHPP
#define PARSERHPP

#include <string>
//#include "../mysql_connector/MySQL.hpp"

class Parser {
private:
public:
    bool init();
    bool auth(std::string login, std::string password);
};

#endif